# gitlab-exporter

## Description

gitlab-exporter extracts metrics from GitLab using GraphQL API and outputs them in a prometheus compatible format

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## Usage

gitlab-exporter requires the following environment variables to be set:

* GITLAB_EXPORTER_GITLAB_API_URL - Gitlab GraphQL API url (defaults to `https://gitlab.archlinux.org/api/graphql`)
* GITLAB_EXPORTER_GITLAB_TOKEN - Create one from Gitlab UI
![Access Token](docs/admin_token_ro.png?raw=true "Access Token")

### Examples
#### usage
```shell

./target/release/gitlab-exporter
gitlab-exporter 0.1.0
Extracts metrics from GitLab's GraphQL API and produce prometheus consumable metrics

USAGE:
    gitlab-exporter [FLAGS] [OPTIONS] <SUBCOMMAND>

FLAGS:
    -h, --help            Prints help information
    -p, --progress-bar    Show progress indicator
    -V, --version         Prints version information
    -v, --verbose         Verbose mode (-v, -vv, -vvv, etc.)

OPTIONS:
    -o <output>        Output file, stdout if not present

SUBCOMMANDS:
    cli     Execute once and output in cli
    help    Prints this message or the help of the given subcommand(s)
```
#### cli (stdout)
```shell
export GITLAB_EXPORTER_GITLAB_API_URL="https://gitlab.archlinux.org/api/graphql" 
export GITLAB_EXPORTER_GITLAB_TOKEN="admin-read-api-token" 
./target/debug/gitlab-exporter cli
# HELP gitlab_exporter_total_build_artifacts_size gitlab exporter total build artifacts size
# TYPE gitlab_exporter_total_build_artifacts_size gauge
gitlab_exporter_total_build_artifacts_size 0
# HELP gitlab_exporter_total_commit_count gitlab exporter total commit count
# TYPE gitlab_exporter_total_commit_count gauge
gitlab_exporter_total_commit_count 8
# HELP gitlab_exporter_total_lfs_objects_size gitlab exporter total lfs objects size
# TYPE gitlab_exporter_total_lfs_objects_size gauge
gitlab_exporter_total_lfs_objects_size 0
# HELP gitlab_exporter_total_packages_size gitlab exporter total packages size
# TYPE gitlab_exporter_total_packages_size gauge
gitlab_exporter_total_packages_size 0
# HELP gitlab_exporter_total_pipeline_artifacts_size gitlab exporter total pipeline artifacts size
# TYPE gitlab_exporter_total_pipeline_artifacts_size gauge
gitlab_exporter_total_pipeline_artifacts_size 0
# HELP gitlab_exporter_total_repository_size gitlab exporter total repository size
# TYPE gitlab_exporter_total_repository_size gauge
gitlab_exporter_total_repository_size 230686
# HELP gitlab_exporter_total_snippets_size gitlab exporter total snippets size
# TYPE gitlab_exporter_total_snippets_size gauge
gitlab_exporter_total_snippets_size 0
# HELP gitlab_exporter_total_storage_size gitlab exporter total storage size
# TYPE gitlab_exporter_total_storage_size gauge
gitlab_exporter_total_storage_size 241171
# HELP gitlab_exporter_total_uploads_size gitlab exporter total uploads size
# TYPE gitlab_exporter_total_uploads_size gauge
gitlab_exporter_total_uploads_size 0
# HELP gitlab_exporter_total_wiki_size gitlab exporter total wiki size
# TYPE gitlab_exporter_total_wiki_size gauge
gitlab_exporter_total_wiki_size 10485
# HELP gitlab_exporter_trends_total_groups gitlab exporter trends total groups
# TYPE gitlab_exporter_trends_total_groups gauge
gitlab_exporter_trends_total_groups 1
# HELP gitlab_exporter_trends_total_projects gitlab exporter trends total projects
# TYPE gitlab_exporter_trends_total_projects gauge
gitlab_exporter_trends_total_projects 2
# HELP gitlab_exporter_trends_total_users gitlab exporter trends total users
# TYPE gitlab_exporter_trends_total_users gauge
gitlab_exporter_trends_total_users 5
```