use std::convert::TryInto;
use std::error::Error;

use graphql_client::{GraphQLQuery, Response};

use crate::graphql::{GraphQL, Metrics};

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/schema.json",
    query_path = "graphql/queries.graphql",
    response_derives = "Serialize,PartialEq, Debug",
    variable_derives = "Serialize,PartialEq, Debug",
    normalization = "rust"
)]
pub struct RepositorySize {
    graph_q_l: GraphQL,
}

impl RepositorySize {
    pub fn new() -> Self {
        RepositorySize {
            graph_q_l: GraphQL::new().unwrap(),
        }
    }
    pub async fn run(&self) -> Result<Metrics, Box<dyn Error + Send + Sync>> {
        let mut repository_size: f64 = 0.0;
        let mut build_artifacts_size: f64 = 0.0;
        let mut commit_count: f64 = 0.0;
        let mut storage_size: f64 = 0.0;
        let mut lfs_objects_size: f64 = 0.0;
        let mut packages_size: f64 = 0.0;
        let mut pipeline_artifacts_size: f64 = 0.0;
        let mut snippets_size: f64 = 0.0;
        let mut uploads_size: f64 = 0.0;
        let mut wiki_size: f64 = 0.0;

        let mut more_data: bool;
        let mut end_cursor = String::new();

        let mut projects = self
            .query(end_cursor)
            .await
            .unwrap()
            .data
            .expect("missing response data")
            .projects
            .expect("missing projects");
        if let Some(e) = projects.edges {
            e.iter()
                .flatten()
                .map(|edge| edge.node.as_ref().unwrap().statistics.as_ref().unwrap())
                .for_each(|stats| {
                    build_artifacts_size += stats.build_artifacts_size;
                    commit_count += stats.commit_count;
                    lfs_objects_size += stats.lfs_objects_size;
                    packages_size += stats.packages_size;
                    pipeline_artifacts_size += stats.pipeline_artifacts_size.unwrap_or(0.0);
                    repository_size += stats.repository_size;
                    snippets_size += stats.snippets_size.unwrap_or(0.0);
                    storage_size += stats.storage_size;
                    uploads_size += stats.uploads_size.unwrap_or(0.0);
                    wiki_size += stats.wiki_size.unwrap_or(0.0);
                })
        }
        more_data = projects.page_info.has_next_page;
        while more_data {
            end_cursor = projects
                .page_info
                .end_cursor
                .as_ref()
                .map(String::from)
                .unwrap();
            projects = self
                .query(end_cursor)
                .await
                .unwrap()
                .data
                .expect("missing response data")
                .projects
                .expect("missing projects");
            if let Some(e) = projects.edges {
                e.iter()
                    .flatten()
                    .map(|edge| edge.node.as_ref().unwrap().statistics.as_ref().unwrap())
                    .for_each(|stats| {
                        build_artifacts_size += stats.build_artifacts_size;
                        commit_count += stats.commit_count;
                        lfs_objects_size += stats.lfs_objects_size;
                        packages_size += stats.packages_size;
                        pipeline_artifacts_size += stats.pipeline_artifacts_size.unwrap_or(0.0);
                        repository_size += stats.repository_size;
                        snippets_size += stats.snippets_size.unwrap_or(0.0);
                        storage_size += stats.storage_size;
                        uploads_size += stats.uploads_size.unwrap_or(0.0);
                        wiki_size += stats.wiki_size.unwrap_or(0.0);
                    })
            }
            more_data = projects.page_info.has_next_page;
        }
        let mut metrics = Metrics::new();
        metrics.add_metric(
            String::from("total_build_artifacts_size"),
            (build_artifacts_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_commit_count"),
            (commit_count as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_lfs_objects_size"),
            (lfs_objects_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_packages_size"),
            (packages_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_pipeline_artifacts_size"),
            (pipeline_artifacts_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_repository_size"),
            (repository_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_snippets_size"),
            (snippets_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_storage_size"),
            (storage_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_uploads_size"),
            (uploads_size as i64).try_into().unwrap(),
        );
        metrics.add_metric(
            String::from("total_wiki_size"),
            (wiki_size as i64).try_into().unwrap(),
        );
        Ok(metrics)
    }

    async fn query(
        &self,
        cursor: String,
    ) -> Result<Response<repository_size::ResponseData>, reqwest::Error> {
        let response = self
            .graph_q_l
            .client()
            .post(self.graph_q_l.gitlab_url())
            .json(&RepositorySize::build_query(repository_size::Variables {
                page_size: self.graph_q_l.page_size(),
                end_cursor: cursor,
            }))
            .send()
            .await?
            .json::<Response<repository_size::ResponseData>>()
            .await?;
        Ok(response)
    }
}
