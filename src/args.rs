use std::path::PathBuf;

use structopt::StructOpt;

/// Extracts metrics from GitLab's GraphQL API and produce prometheus consumable metrics
#[derive(Debug, StructOpt)]
pub struct Args {
    #[structopt(flatten)]
    pub command: Command,
    /// Show progress indicator
    #[structopt(short, long)]
    pub progress_bar: bool,
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short, long, parse(from_occurrences), alias = "debug")]
    pub verbose: u8,
    /// Output file, stdout if not present
    #[structopt(short, parse(from_os_str))]
    pub output: Option<PathBuf>,
    // /// GraphQL API url
    // #[structopt(
    //     short = "a",
    //     long,
    //     env = "API_URL",
    //     default_value = "https://gitlab.archlinux.org/api/graphql"
    // )]
    // pub api_url: String,
    // /// API token
    // #[structopt(short = "t", long, env = "API_TOKEN")]
    // pub api_token: String,
}

#[derive(Debug, StructOpt)]
pub enum Command {
    /// Execute once and output in cli
    Cli,
    //TODO: implement http server
    // /// Server metrics over http server
    //Http,
}
