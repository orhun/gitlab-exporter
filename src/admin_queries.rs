use std::convert::TryInto;
use std::error::Error;

use graphql_client::{GraphQLQuery, Response};
use reqwest::Client;

use crate::graphql::{GraphQL, Metrics};

type Time = String;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/schema.json",
    query_path = "graphql/queries.graphql",
    response_derives = "Serialize,PartialEq, Debug",
    normalization = "rust"
)]
pub struct UsersCount {
    graph_q_l: GraphQL,
}

impl UsersCount {
    pub fn new() -> Self {
        UsersCount {
            graph_q_l: GraphQL::new().unwrap(),
        }
    }
    pub async fn run(&self) -> Result<Metrics, Box<dyn Error + Send + Sync>> {
        let mut metrics = Metrics::new();
        if let Some(utm) = self
            .query(self.graph_q_l.client(), users_count::Variables {})
            .await
            .unwrap()
            .data
            .expect("missing response data")
            .usage_trends_measurements
        {
            let mut count: i64 = 0;
            if let Some(e) = utm.edges {
                count += e
                    .iter()
                    .flatten()
                    .map(|trends| trends.node.as_ref().unwrap())
                    .next()
                    .unwrap()
                    .count
            }
            metrics.add_metric(
                String::from("trends_total_users"),
                count.try_into().unwrap(),
            )
        }
        Ok(metrics)
    }

    async fn query(
        &self,
        client: &Client,
        variables: users_count::Variables,
    ) -> Result<Response<users_count::ResponseData>, reqwest::Error> {
        let request_body = UsersCount::build_query(variables);
        let response = client
            .post(self.graph_q_l.gitlab_url())
            .json(&request_body)
            .send()
            .await?
            .json::<Response<users_count::ResponseData>>()
            .await?;
        Ok(response)
    }
}

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/schema.json",
    query_path = "graphql/queries.graphql",
    response_derives = "Serialize,PartialEq, Debug",
    normalization = "rust"
)]
pub struct ProjectsCount {
    graph_q_l: GraphQL,
}

impl ProjectsCount {
    pub fn new() -> Self {
        ProjectsCount {
            graph_q_l: GraphQL::new().unwrap(),
        }
    }
    pub async fn run(&self) -> Result<Metrics, Box<dyn Error + Send + Sync>> {
        let mut metrics = Metrics::new();
        if let Some(utm) = self
            .query(self.graph_q_l.client(), projects_count::Variables {})
            .await
            .unwrap()
            .data
            .expect("missing response data")
            .usage_trends_measurements
        {
            let mut count: i64 = 0;
            if let Some(e) = utm.edges {
                count += e
                    .iter()
                    .flatten()
                    .map(|trends| trends.node.as_ref().unwrap())
                    .next()
                    .unwrap()
                    .count
            }
            metrics.add_metric(
                String::from("trends_total_projects"),
                count.try_into().unwrap(),
            )
        }

        Ok(metrics)
    }

    async fn query(
        &self,
        client: &Client,
        variables: projects_count::Variables,
    ) -> Result<Response<projects_count::ResponseData>, reqwest::Error> {
        let request_body = ProjectsCount::build_query(variables);
        let response = client
            .post(self.graph_q_l.gitlab_url())
            .json(&request_body)
            .send()
            .await?
            .json::<Response<projects_count::ResponseData>>()
            .await?;
        Ok(response)
    }
}

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/schema.json",
    query_path = "graphql/queries.graphql",
    response_derives = "Serialize,PartialEq, Debug",
    normalization = "rust"
)]
pub struct GroupsCount {
    graph_q_l: GraphQL,
}

impl GroupsCount {
    pub fn new() -> Self {
        GroupsCount {
            graph_q_l: GraphQL::new().unwrap(),
        }
    }
    pub async fn run(&self) -> Result<Metrics, Box<dyn Error + Send + Sync>> {
        let mut metrics = Metrics::new();
        if let Some(utm) = self
            .query()
            .await
            .unwrap()
            .data
            .expect("missing response data")
            .usage_trends_measurements
        {
            let mut count: i64 = 0;
            if let Some(e) = utm.edges {
                count += e
                    .iter()
                    .flatten()
                    .map(|trends| trends.node.as_ref().unwrap())
                    .next()
                    .unwrap()
                    .count
            }
            metrics.add_metric(
                String::from("trends_total_groups"),
                count.try_into().unwrap(),
            )
        }
        Ok(metrics)
    }

    async fn query(&self) -> Result<Response<groups_count::ResponseData>, reqwest::Error> {
        let response = self
            .graph_q_l
            .client()
            .post(self.graph_q_l.gitlab_url())
            .json(&GroupsCount::build_query(groups_count::Variables {}))
            .send()
            .await?
            .json::<Response<groups_count::ResponseData>>()
            .await?;
        Ok(response)
    }
}
