use std::convert::TryInto;
use std::fs;
use std::sync::Arc;
use std::time::Instant;

use anyhow::Result;
use env_logger::Env;
use indicatif::{HumanDuration, ProgressBar, ProgressStyle};
use lazy_static::lazy_static;
use log::{debug, error};
use prometheus::{Encoder, IntGauge, Opts, Registry, TextEncoder};
use structopt::StructOpt;
use warp::{Rejection, Reply};

use args::Args;

use crate::admin_queries::{GroupsCount, ProjectsCount, UsersCount};
use crate::args::Command;
use crate::queries::RepositorySize;

mod admin_queries;
mod args;
mod graphql;
mod queries;

#[allow(dead_code)]
const DEFAULT_PAUSE_BETWEEN_QUERIES: u64 = 300; // 5 minutes

lazy_static! {
    pub static ref TOTAL_BUILD_TRENDS_TOTAL_USER_GAUGE: IntGauge =
        IntGauge::new("gitlab_exporter_trends_total_users", "trends total users")
            .expect("metric can be created");
    pub static ref TOTAL_BUILD_ARTIFACTS_SIZE_GAUGE: IntGauge = IntGauge::new(
        "gitlab_exporter_total_build_artifacts_size",
        "total build artifacts size"
    )
    .expect("metric can be created");
    pub static ref REGISTRY: Registry = Registry::new();
}

#[tokio::main]
async fn main() {
    let args = Args::from_args();
    let logging = match args.verbose {
        0 => "info",
        1 => "gitlab-exporter=debug",
        _ => "debug",
    };

    env_logger::init_from_env(Env::default().default_filter_or(logging));
    debug!("{:?}", args);

    if let Err(err) = run(args).await {
        error!("Error: {:?}", err);
        for cause in err.chain() {
            error!("Caused by: {:?}", cause)
        }
        std::process::exit(1)
    }
    std::process::exit(0)
}

async fn run(args: Args) -> Result<()> {
    match args.command {
        Command::Cli => {
            let started = Instant::now();
            let pb: Option<ProgressBar> = initialize_progress_bar(&args);

            let handles = vec![
                tokio::spawn(async move { Arc::new(UsersCount::new()).run().await }),
                tokio::spawn(async move { Arc::new(ProjectsCount::new()).run().await }),
                tokio::spawn(async move { Arc::new(GroupsCount::new()).run().await }),
                tokio::spawn(async move { Arc::new(RepositorySize::new()).run().await }),
            ];
            let results = futures::future::join_all(handles).await.into_iter();
            results.flatten().flatten().for_each(|m| {
                for (key, value) in m.get_metrics() {
                    let help_text = key.to_string().replace("_", " ");
                    let gauge = IntGauge::with_opts(Opts::new(key, help_text)).unwrap();
                    REGISTRY.register(Box::new(gauge.clone())).unwrap();
                    gauge.set(usize::try_into(value).unwrap());
                }
            });
            if let Some(pb) = pb {
                pb.finish_with_message(format!("Done in {}", HumanDuration(started.elapsed())));
            }
            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            let metric_families = REGISTRY.gather();
            encoder.encode(&metric_families, &mut buffer).unwrap();
            match args.output {
                Some(output) => fs::write(output, buffer).expect("Unable to write file"),
                None => println!("{}", String::from_utf8(buffer).unwrap()),
            }
        } // Command::Http => {
          //     panic!("Not implemented feature.");
          // let health_route = warp::path!("health").map(|| StatusCode::PROCESSING);
          // let metrics_route = warp::path!("metrics").and_then(metrics_handler);
          //
          // warp::serve(metrics_route.or(health_route))
          //     .run(([0, 0, 0, 0], 8080))
          //     .await;
          // }
    }
    Ok(())
}

#[allow(dead_code)]
async fn metrics_handler() -> Result<impl Reply, Rejection> {
    let encoder = TextEncoder::new();

    let mut buffer = Vec::new();
    if let Err(e) = encoder.encode(&REGISTRY.gather(), &mut buffer) {
        error!("could not encode custom metrics: {}", e);
    };
    let res = match String::from_utf8(buffer.clone()) {
        Ok(v) => v,
        Err(e) => {
            error!("custom metrics could not be from_utf8'd: {}", e);
            String::default()
        }
    };
    buffer.clear();
    Ok(res)
}

fn initialize_progress_bar(args: &Args) -> Option<ProgressBar> {
    if args.progress_bar {
        debug!("Initializing progress indicator");
        let pb = ProgressBar::new_spinner();
        pb.enable_steady_tick(200);
        pb.set_style(
            ProgressStyle::default_spinner()
                .tick_strings(&[".  ", ".. ", "...", " ..", "  .", "   "])
                .template("{spinner:.blue} {msg}"),
        );
        pb.set_message("Extracting...");
        Some(pb)
    } else {
        None
    }
}
