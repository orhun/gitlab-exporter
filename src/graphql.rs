use std::collections::HashMap;
use std::env;

use anyhow::Context;
use reqwest::Client;

const DEFAULT_PAGE_SIZE: i64 = 50;
const METRIC_PREFIX: &str = "gitlab_exporter_";

/// User agent that will be used for requests.
static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

pub struct Metrics {
    metrics: HashMap<String, usize>,
}

impl Metrics {
    pub fn new() -> Metrics {
        Metrics {
            metrics: HashMap::new(),
        }
    }
    pub fn add_metric(&mut self, metric_name: String, metric_value: usize) {
        self.metrics
            .insert(format!("{}{}", METRIC_PREFIX, metric_name), metric_value);
    }
    pub fn get_metrics(&self) -> HashMap<String, usize> {
        self.metrics.clone()
    }
}

pub struct GraphQL {
    client: Client,
    page_size: i64,
    gitlab_url: String,
}

impl GraphQL {
    pub fn new() -> Result<GraphQL, anyhow::Error> {
        let gitlab_url = String::from(
            option_env!("GITLAB_EXPORTER_GITLAB_API_URL")
                .unwrap_or("https://gitlab.archlinux.org/api/graphql"),
        );
        let token = &env::var("GITLAB_EXPORTER_GITLAB_TOKEN")
            .context("Missing env var GITLAB_EXPORTER_GITLAB_TOKEN")?;
        let client = Client::builder()
            .user_agent(APP_USER_AGENT)
            .default_headers(
                std::iter::once((
                    reqwest::header::AUTHORIZATION,
                    reqwest::header::HeaderValue::from_str(&format!("Bearer {}", token)).unwrap(),
                ))
                .collect(),
            )
            .build()?;
        Ok(GraphQL {
            client,
            page_size: DEFAULT_PAGE_SIZE,
            gitlab_url,
        })
    }

    pub fn client(&self) -> &Client {
        &self.client
    }

    pub fn page_size(&self) -> i64 {
        self.page_size
    }

    pub fn gitlab_url(&self) -> &String {
        &self.gitlab_url
    }
}
